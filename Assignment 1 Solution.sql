Create database Worker
use Worker

Create table Worker(WORKER_ID INT,
	FIRST_NAME CHAR(25) NOT NUll,
	LAST_NAME CHAR(25),
	SALARY int,
	JOINING_DATE DATETIME,
	DEPARTMENT CHAR(25),
	primary key(WORKER_ID), 
	check(SALARY >= 10000 and SALARY <= 25000), 
	check(DEPARTMENT in ('HR', 'Sales', 'Accts', 'IT'))

)

--Inserting

insert into Worker values(10, 'sadfn', NULL, 24999, '2014-02-14 00:00:00', 'Sales');

--selecting
select * 
from Worker

--Q-1. Write an SQL query to fetch �FIRST_NAME� from Worker table using the alias name as <WORKER_NAME>.

select FIRST_NAME Worker_Name 
from Worker


--Q-2. Write an SQL query to fetch �FIRST_NAME� from Worker table in upper case.

select UPPER(FIRST_NAME) 
from Worker


--Q-3. Write an SQL query to fetch unique values of DEPARTMENT from Worker table.
select DEPARTMENT
from Worker
group by DEPARTMENT

 
--Q-4. Write an SQL query to print the first three characters of  FIRST_NAME from Worker table.

select Substring(FIRST_NAME,1,  3)
from Worker


 
--Q-5. Write an SQL query to find the position of the alphabet (�a�) in the first name column �Amitabh� from Worker table.

select CharIndex('a', FIRST_NAME ) as INDEX_A
from Worker
 
--Q-6. Write an SQL query to print the FIRST_NAME from Worker table after removing white spaces from the right side.

select RTRIM(FIRST_NAME )
from Worker
 
--Q-7. Write an SQL query to print the DEPARTMENT from Worker table after removing white spaces from the left side.

select LTRIM(DEPARTMENT )
from Worker
 
--Q-8. Write an SQL query that fetches the unique values of DEPARTMENT from Worker table and prints its length.
select DEPARTMENT, len(DEPARTMENT) as Lenght_Dept
from Worker
group by DEPARTMENT
 
--Q-9. Write an SQL query to print the FIRST_NAME from Worker table after replacing �a� with �A�.
select Replace(FIRST_NAME , 'a', 'A') as FIRST_NAME
from Worker
 
--Q-10. Write an SQL query to print the FIRST_NAME and LAST_NAME from Worker table into a single column COMPLETE_NAME. A space char should separate them.

select Concat(TRIM(FIRST_NAME),' ' ,  TRIM(LAST_NAME)) as COMPLETE_NAME
from Worker
 
--Q-11. Write an SQL query to print all Worker details from the Worker table order by FIRST_NAME Ascending.
select *
from Worker
order by FIRST_NAME asc

 
--Q-12. Write an SQL query to print all Worker details from the Worker table order by FIRST_NAME Ascending and DEPARTMENT Descending.
select *
from Worker
order by FIRST_NAME, DEPARTMENT desc
 
--Q-13. Write an SQL query to print details for Workers with the first name as �Vipul� and �Satish� from Worker table.
select *
from Worker
where FIRST_NAME in('Vipul' , 'Satish')
 
--Q-14. Write an SQL query to print details of workers excluding first names, �Vipul� and �Satish� from Worker table.
select *
from Worker
where FIRST_NAME not in('Vipul' , 'Satish')
 
--Q-15. Write an SQL query to print details of Workers with DEPARTMENT name as �Admin�.
select *
from Worker
where DEPARTMENT  in('Admin')

 
--Q-16. Write an SQL query to print details of the Workers whose FIRST_NAME contains �a�.
select *
from Worker
where FIRST_NAME like '%a%'

--Q-17. Write an SQL query to print details of the Workers whose FIRST_NAME ends with �a�.

select *
from Worker
where FIRST_NAME like '%a'
 
--Q-18. Write an SQL query to print details of the Workers whose FIRST_NAME ends with �h� and contains six alphabets.

select *
from Worker
where FIRST_NAME like '%h' and len(FIRST_NAME)=6

--Q-19. Write an SQL query to print details of the Workers whose SALARY lies between 100000 and 500000.
select *
from Worker
where SALARY between 100000 and 500000
 
--Q-20. Write an SQL query to print details of the Workers who have joined in Feb�2014.
select *
from Worker
where MONTH(JOINING_DATE)=2 and YEAR(JOINING_DATE)=2014

 
--Q-21. Write an SQL query to fetch the count of employees working in the department �Admin�.
select count(DEPARTMENT) as COUNT_ADMIN
from Worker
where DEPARTMENT='Admin'

